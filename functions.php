<?php

/* Enqueue Mantra original stylesheet */
add_action('wp_enqueue_scripts', 'mantra_css');
function mantra_css() {
	wp_enqueue_style('mantra', get_template_directory_uri().'/style.css');
}

/* Opt out for suggesting the persistent object cache */
add_filter('site_status_should_suggest_persistent_object_cache', '__return_false');

function mantra_lmcz_title_and_description() {
	global $mantra_options;
	extract( $mantra_options );

	printf( '<div class="even">' );
	printf( '<img class="flag" alt="cz" src="%1$s" />', get_stylesheet_directory_uri().'/images/flag-cz.svg' );
	printf( '<img class="mint" alt="%1$s" title="%1$s" src="%2$s" />', esc_attr( get_bloginfo( 'name', 'display' ) ), get_stylesheet_directory_uri().'/images/ring-name-mono-white.svg' );
	printf( '<img class="flag" alt="cz" src="%1$s" />', get_stylesheet_directory_uri().'/images/flag-sk.svg' );
	printf( '</div>' );
	?>

	<div id="header-container">
		<a href="<?php echo esc_url( home_url( '/' ) ) ?>" id="linky"></a>
		<?php
		if ($mantra_socialsdisplay0): mantra_header_socials(); endif;
		?>
	</div> <!-- #header-container -->
	<?php

} // mantra_title_and_description()
add_action( 'cryout_branding_hook', 'mantra_lmcz_title_and_description' );

add_action( 'init', static function () {
	remove_action( 'cryout_branding_hook', 'mantra_title_and_description' );
} );